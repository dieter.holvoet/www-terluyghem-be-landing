import { format, subHours, parse } from 'date-fns';

export const parseMs = value => ({
    years: value / (86400000 * 365),
    days: value / 86400000,
    hours: (value / 3600000) % 24,
    minutes: (value / 60000) % 60,
    seconds: (value / 1000) % 60,
    milliseconds: value % 1000,
});

export const getDuration = (millis, locale) => {
    const { years, days, hours, minutes } = parseMs(millis);
    const round = millis > 0 ? Math.floor : Math.ceil;

    if (Math.floor(years)) {
        return {
            value: days.toFixed(),
            unit: locale === 'nl' ? 'jaar' : 'years',
        };
    }

    if (Math.floor(days)) {
        return {
            value: days.toFixed(),
            unit: locale === 'nl' ? 'dagen' : 'days',
        };
    }

    if (Math.floor(hours)) {
        return {
            value: round(hours),
            unit: locale === 'nl' ? 'uur' : 'hours',
        };
    }

    return {
        value: round(minutes),
        unit: locale === 'nl' ? 'minuten' : 'minutes',
    };
};

export const formatDuration = (millis, locale) => {
    const { years, days, hours, minutes, seconds } = parseMs(millis);
    const round = millis > 0 ? Math.floor : Math.ceil;
    const pad = num => num.toFixed().padStart(2, '0');

    if (Math.floor(years)) {
        if (locale === 'nl') {
            return `Nog ${days.toFixed()} jaar`;
        }

        return `${days.toFixed()} years left`;
    }

    if (Math.floor(days)) {
        if (locale === 'nl') {
            return `Nog ${days.toFixed()} dagen`;
        }

        return `${days.toFixed()} days left`;
    }

    if (Math.floor(hours)) {
        return `${pad(round(hours))}:${pad(minutes)}:${pad(seconds)}`;
    }

    return `${pad(round(minutes))}:${pad(seconds)}`;
};

export const formatOffset = offset => `${offset > 0 ? '+' : '-'}${String(offset).padStart(2)}:00`;

export const formatHourRange = ({ startDate, endDate, eventTimezoneOffset }) => {
    const clientTimezoneOffset = new Date().getTimezoneOffset() / -60;
    let start = parse(startDate * 1000);
    let end = parse(endDate * 1000);

    if (clientTimezoneOffset !== eventTimezoneOffset) {
        start = subHours(start, clientTimezoneOffset - eventTimezoneOffset);
        end = subHours(end, clientTimezoneOffset - eventTimezoneOffset);
        return `${format(start, 'HH:mm')} - ${format(end, 'HH:mm')} (GMT ${formatOffset(eventTimezoneOffset)})`;
    }

    return `${format(start, 'HH:mm')} - ${format(end, 'HH:mm')}`;
};

import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { differenceInMilliseconds } from 'date-fns';

import { getDuration } from './date';

class Countdown extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            millis: 0,
        };
    }

    componentDidMount() {
        this.update();
        this.interval = window.setInterval(this.update, 1000);
    }

    componentWillUnmount() {
        window.clearInterval(this.interval);
    }

    update = () => {
        const { end } = this.props;

        let now = Date.now();
        if ('date' in queryString.parse(window.location.search)) {
            now = queryString.parse(window.location.search).date;
        }

        this.setState({
            millis: differenceInMilliseconds(end, now),
        });
    };

    buildText = () => {
        const { millis } = this.state;
        const { text, locale } = this.props;
        const { value, unit } = getDuration(millis, locale);

        if (!text[locale]) {
            throw new Error(`No text specified for locale '${locale}'`);
        }

        return text[locale]
            .replace(':unit', unit)
            .replace(':value', value);
    };

    render() {
        const { millis } = this.state;

        return (
            <p className="countdown" style={{ opacity: millis === 0 ? 0 : 1 }}>
                {this.buildText()}
            </p>
        );
    }
}

Countdown.propTypes = {
    end: PropTypes.string.isRequired,
    locale: PropTypes.string.isRequired,
};

export default Countdown;

import React from 'react';
import ReactDOM from 'react-dom';
import domLoaded from 'dom-loaded';

import Countdown from './Countdown';

domLoaded.then(() => {
    const container = document.querySelector('[data-countdown]');

    if (container) {
        ReactDOM.render(
            <Countdown
                end="2019-03-11T05:00:00"
                locale={document.documentElement.lang}
                text={{
                    en: ':value :unit left until launch',
                    nl: 'Nog :value :unit voor opening!',
                }}
            />,
            container,
        );
    }
});

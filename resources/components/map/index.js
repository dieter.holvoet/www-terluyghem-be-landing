import poppy from '~/public/images/poppy.svg';
import styles from './mapStyles';

const init = () => {
    const map = new google.maps.Map(document.querySelector('.map__container'), {
        center: { lat: 50.963836, lng: 2.837754 },
        mapTypeControl: false,
        fullscreenControl: false,
        zoom: 15,
        styles,
    });

    const marker = new google.maps.Marker({
        position: { lat: 50.963836, lng: 2.837754 },
        icon: poppy,
        map,
    });
}

google.maps.event.addDomListener(window, 'load', init);